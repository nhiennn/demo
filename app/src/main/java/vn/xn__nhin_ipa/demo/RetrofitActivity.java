package vn.xn__nhin_ipa.demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import vn.xn__nhin_ipa.demo.Adapter.EntityAdapter;

public class RetrofitActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrofit);
    }

    public void clickRequest(View view) {
        EntityAdapter entityAdapter = new EntityAdapter();
        entityAdapter.start();
    }
}
