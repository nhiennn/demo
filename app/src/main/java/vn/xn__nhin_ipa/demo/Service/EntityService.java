package vn.xn__nhin_ipa.demo.Service;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import vn.xn__nhin_ipa.demo.Entity.Entity;

/**
 * Created by ngocn on 06-Dec-17.
 */

public interface EntityService {
    @GET("users")
    Call<List<Entity>> getAllEntity();
}
