package vn.xn__nhin_ipa.demo.Adapter;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import vn.xn__nhin_ipa.demo.Entity.Entity;

import retrofit2.Callback;
import vn.xn__nhin_ipa.demo.Service.EntityService;

/**
 * Created by ngocn on 06-Dec-17.
 */

public class EntityAdapter implements Callback<List<Entity>> {

    static final String BASE_URL = "https://api.github.com/";

    public void start() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        EntityService entityService = retrofit.create(EntityService.class);
        Call<List<Entity>> call = entityService.getAllEntity();
        call.enqueue(this);
    }


    @Override
    public void onResponse(Call<List<Entity>> call, Response<List<Entity>> response) {
        if (response.isSuccessful()) {
            List<Entity> entityList = response.body();

            for (Entity entity: entityList) {
                System.out.println(entity.getAvatarUrl());
            }

            Log.e("!!!!!!!!!!!!!!", "done");

//            entityList.forEach(change -> System.out.println(change.subject));
//            for (Object entity: entityList) {
//                Log.e("Data", entity.getAvatarUrl());
//            }
        } else {
            Log.e("ERROR", "LOI CMNR!");
        }
    }

    @Override
    public void onFailure(Call<List<Entity>> call, Throwable t) {
        Log.e("ERROR", "LOI CMNR!");
        t.printStackTrace();
    }
}
